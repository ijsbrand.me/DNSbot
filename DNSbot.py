import requests
import json

text = """"text":"""
personID = """","personId":"""

def GetMessages():
    global messageText
    url = "https://api.ciscospark.com/v1/messages"
    querystring = {"roomId":"Y2lzY29zcGFyazovL3VzL1JPT00vYTk3YmZmNjAtYjMyMC0xMWU2LTkxY2ItYjM5YjYyYTNmYjNj","mentionedPeople":"me"}
    headers = {
        'authorization': "Bearer MjE4ZmQ1MTctYTgyZi00NzJhLTlkY2EtZTFmMDM2ZjQ4NGJhZTc4YWRjZTAtY2M5",
        'cache-control': "no-cache",
        'postman-token': "a8f93ddc-5107-89e9-b81d-a4c1c2932b30"
        }
    response = requests.request("GET", url, headers=headers, params=querystring)
    print(response.text)
    
    newString = response.text
    beginText = newString.find(text)+8
    endText = newString.find(personID)
    messageText = newString[beginText:endText]
    
    FindURL()

def PostMessages():
    global replyText
    url = "https://api.ciscospark.com/v1/messages"

    querystring = {"roomId":"Y2lzY29zcGFyazovL3VzL1JPT00vYTk3YmZmNjAtYjMyMC0xMWU2LTkxY2ItYjM5YjYyYTNmYjNj","text":"Test 2"}

    headers = {
        'authorization': "Bearer MjE4ZmQ1MTctYTgyZi00NzJhLTlkY2EtZTFmMDM2ZjQ4NGJhZTc4YWRjZTAtY2M5",
        'content-type': "application/json; charset=utf-8",
        'cache-control': "no-cache",
        'postman-token': "bad2de62-b037-cdc8-ce0c-92ec7f521dfa"
        }
    
    payload ={"roomId":"Y2lzY29zcGFyazovL3VzL1JPT00vYTk3YmZmNjAtYjMyMC0xMWU2LTkxY2ItYjM5YjYyYTNmYjNj","text":replyText}

    response = requests.request("POST", url, headers=headers, params=querystring, data=json.dumps(payload))

    print(response.text) 

def DNScheck ():
    global result
    url = "https://investigate.api.umbrella.com/domains/categorization/" + result

    headers = {
        'content-type': "application/json; charset=utf-8",
        'authorization': "Bearer 6d72f6a3-6575-42fc-bd13-0e43260476d3",
        'cache-control': "no-cache",
        'postman-token': "999a0549-14b0-826f-8a31-b3133617d1e2"
        }

    response = requests.request("GET", url, headers=headers)
    string = response.text

    result = string[string.find ('status') + 8]
    
    ResponseText()

def FindURL():
    global messageText
    global result
    global replyText
    triggerStart = "www."
    endTrigger = ".com"

    newString = messageText
    beginText = newString.find(triggerStart)+len(triggerStart)
    endText = newString.find(endTrigger)+len(endTrigger)
    result = newString[beginText:endText]
    
    if(newString.find(triggerStart) == -1):
        replyText = "Thanks for the shoutout, but if there's no link, I won't be of any value (maybe add www. to the url)"
        PostMessages()
    else:
        DNScheck()

def ResponseText():
    global result
    print(result)
    global replyText
    if(result == "-"):
        replyText = "Malicious page detected, enter at your own peril"
    elif(result == "0"):
        replyText = "No idea what that link is, good luck!"
    elif(result == "1"):
        replyText = "The link is safe, enjoy your browsing"
    else:
        replyText = "Error, my code broke, self destruct in 3... 2... 1...    <Boom>"
        
    PostMessages()

GetMessages()



 
    
